# nds conda recipe

Home: https://gitlab.esss.lu.se/epics-modules/nds/

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS NDS module
